## Background

PHP application using design pattern mvc,solid principles,composer for dependencies
and docker-compose without any framework for setting up the environment with Apache2 and PHP8
and it provides a clean code with PSR0 and PSR4 for autoload,also the
PSR1,PSR2,PSR3

## Run the project
### Setup
- `docker-compose up -d`

## Import seeds
### Setup
- `php source/dbseed.php`

## Tests
this application contains 10 test cases with phpunit
- `docker-compose exec php vendor/bin/phpunit`

Happy reading!
