<?php

namespace CentralPointProductApp\Controllers;

use CentralPointProductApp\Constants\Errors;
use CentralPointProductApp\Constants\Status;
use CentralPointProductApp\Contracts\ControllerInterface;
use CentralPointProductApp\Models\Product;
use CentralPointProductApp\Repository\ProductRepository;

/**
 * The baseContract
 *
 * @category   Contracts
 * @author  Samir GUIDERK <samir.guiderk@gmail.com>
 * @copyright  2022 Centralpoint IO
 * @since      1.0.0
 *
 */
class ProductController implements ControllerInterface
{
  /**
   * @param ProductRepository $productRepository
   * @param $requestMethod
   * @param int $productId
   * @param string $name
   */
  public function __construct(
    protected ProductRepository $productRepository,
    private $requestMethod,
    private int $productId,
    private string $name
  ) {
  }

  /**
   * @return string
   */
  public function processRequest()
  {
    switch ($this->requestMethod) {
      case 'GET':
        if ($this->productId) {
          $response = $this->get($this->productId);
        } else if ($this->name) {
          $response = $this->getAllByCondition($this->name);
        }
        else {
          $response = $this->getAll();
        };
        break;
      case 'POST':
        $response = $this->createFromRequest();
        break;
      case 'PUT':
        $response = $this->updateFromRequest($this->productId);
        break;
      case 'DELETE':
        $response = $this->delete($this->productId);
        break;
      default:
        $response = $this->notFoundResponse();
        break;
    }
    header($response['status_code_header']);
    if ($response['body']) {
      print($response['body']);
    }
  }

  /**
   * @return array
   */
  public function notFoundResponse(): array
  {
    $response['status_code_header'] = Status::CODE_HEADER_NOT_FOUND;
    $response['body'] = null;
    return $response;
  }

  /**
   * @return array
   */
  public function createFromRequest(): array
  {
    $input = $_POST;

    if (!$this->validateProduct($input)) {
      return $this->unprocessableEntityResponse();
    }
    $this->insert($input);
    $response['status_code_header'] = Status::CODE_HEADER_SUCCESS_ADD;
    $response['body'] = null;

    return $response;
  }

  /**
   * @param $id
   * @return array
   */
  public function updateFromRequest($id): array
  {
    $result = $this->productRepository->find($id);
    $params = [];
    if (!$result) {
      return $this->notFoundResponse();
    }
    $input = parse_str(file_get_contents('php://input'),$params);

    if (!$this->validateProduct($params)) {
      return $this->unprocessableEntityResponse();
    }
    $this->productRepository->update($id, $params);
    $response['status_code_header'] = Status::CODE_HEADER_SUCCESS;
    $response['body'] = 'product updated';

    return $response;
  }

  /**
   * @param int $id
   * @return array
   */
  public function get(int $id): array
  {
    $result = $this->productRepository->find($id);
    if (!$result) {
      return $this->notFoundResponse();
    }

    $response['status_code_header'] = Status::CODE_HEADER_SUCCESS;
    $response['body'] = json_encode($result);

    return $response;
  }

  /**
   * @return array
   */
  public function getAll(): array
  {
    $result = $this->productRepository->findAll();
    $response['status_code_header'] = Status::CODE_HEADER_SUCCESS;
    $response['body'] = json_encode($result);

    return $response;
  }

  /**
   * @param $params
   * @return array
   */
  public function getAllByCondition($params = null): array
  {
    $result = $this->productRepository->findByCondition("name LIKE '$params' ");
    $response['status_code_header'] = Status::CODE_HEADER_SUCCESS;
    $response['body'] = json_encode($result);

    return $response;
  }

  /**
   * @param $id
   * @return array
   */
  public function delete($id): array
  {
    $result = $this->productRepository->find($id);
    if (!$result) {
      return $this->notFoundResponse();
    }
    $this->productRepository->remove($id);
    $response['status_code_header'] = Status::CODE_HEADER_SUCCESS;
    $response['body'] = null;

    return $response;
  }

  /**
   * @param array $input
   * @return bool
   */
  public function validateProduct(array $input): bool
  {
    if (!isset($input['name'])) {
      return false;
    }
    if (!isset($input['brand'])) {
      return false;
    }
    if (!isset($input['price'])) {
      return false;
    }
    if (!isset($input['description'])) {
      return false;
    }
    if (!isset($input['stock'])) {
      return false;
    }
    return true;
  }

  /**
   * @return array
   */
  public function unProcessableEntityResponse(): array
  {
    $response['status_code_header'] = Status::CODE_HEADER_UNPROCESSABLE_ENTITY;
    $response['status'] = 400;
    $response['body'] = json_encode([
      'error' => Errors::INVALID_INPUT
    ]);

    return $response;
  }

  /**
   * @param $request
   * @return string
   */
  public function insert($request): string
  {
    // Create new product model
    $product = new Product;
    $product->setName($_POST['name']);
    $product->setBrand($_POST['brand']);
    $product->setPrice($_POST['price']);
    $product->setDescription($_POST['description']);
    $product->setStock($_POST['stock']);

    // Save the new product
    $this->productRepository->save($product);

    // Return the id
    return 'Product inserted successfully !';
  }

}
