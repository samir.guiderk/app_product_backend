<?php

namespace CentralPointProductApp\Contracts;

use CentralPointProductApp\Models\Product;

interface ProductRepositoryInterface
{
  /**
   * @param $id
   * @param $data
   * @return string
   */
  public function update($id, $data): string;

  /**
   * @param int $id
   */
  public function find(int $id);

  /**
   * @return array|string
   */
  public function findAll(): array|string;

  /**
   * @param string $condition
   * @return array|string|void
   */
  public function findByCondition(string $condition);

  /**
   * @param Product $product
   * @return string
   */
  public function save(Product $product): string;

  /**
   * @param int $id
   * @return string
   */
  public function remove(int $id): string;

}
