<?php

namespace CentralPointProductApp\Contracts;

interface ControllerInterface
{
  /**
   * @return string
   */
  public function processRequest();

  /**
   * @param int $id
   * @return array
   */
  public function get(int $id): array;

  /**
   * @return array
   */
  public function getAll(): array;

  /**
   * @param $params
   * @return array
   */
  public function getAllByCondition($params = null): array;

  /**
   * @param $id
   * @return array
   */
  public function delete($id): array;

  /**
   * @param $request
   * @return string
   */
  public function insert($request): string;

}
