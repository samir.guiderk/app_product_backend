<?php

namespace CentralPointProductApp\Constants;

/**
 * The Db config
 *
 * @category   Db
 * @author  Samir GUIDERK <samir.guiderk@gmail.com>
 * @copyright  2022 Centralpoint IO
 * @since      1.0.0
 *
 */
class Config
{
    const DB_HOST = 'mysql';
    const DB_USER = 'root';
    const DB_PASS = 'dragon123';
    const DB_NAME = 'db_products';
}
