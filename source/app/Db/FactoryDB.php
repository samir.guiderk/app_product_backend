<?php

namespace CentralPointProductApp\Db;

use CentralPointProductApp\Contracts\BaseModel;
use mysqli;
use CentralPointProductApp\Constants\Config;
use CentralPointProductApp\Constants\Errors;

/**
 * The Db
 *
 * @category   Constants
 * @author  Samir GUIDERK <samir.guiderk@gmail.com>
 * @copyright  2022 Centralpoint IO
 * @since      1.0.0
 *
 */
class FactoryDB
{
    public mysqli $con;

    /**
     * FactoryDB constructor.
     */
    public function __construct()
    {
        if (Config::DB_HOST == "" or Config::DB_USER == "" or Config::DB_PASS == "" or Config::DB_NAME == "") {
            die(Errors::DB_DETAIL_ERROR);
        }
        $this->con = new \mysqli(Config::DB_HOST, Config::DB_USER, Config::DB_PASS, Config::DB_NAME);
        if ($this->con->connect_errno > 0) {
            die(Errors::DB_UNABLE_TO_CONNECT.' :'.$this->con->connect_error);
        }
    }

    /**
     * @return false|\mysqli|null
     */
    public function connect()
    {
        return \mysqli_connect(Config::DB_HOST, Config::DB_USER, Config::DB_PASS, Config::DB_NAME);
    }

    /**
     * @param $conn
     */
    public function deconnect($conn)
    {
        $conn->close();
    }

    /**
     * @param string $sql
     * @return object
     */
    public function query(string $sql): object
    {
        $result = mysqli_query($this->con, $sql) or die(mysqli_error($this->con));

        if ($result->num_rows < 1) {
            return Errors::DB_NO_ROW_FOUND;
        }

        return mysqli_fetch_object($result);
    }

    /**
     * @param string $fields
     * @param string $table
     * @return array
     */
    public function fetchAll(string $fields, string $table): array|string
    {
        $sql = "SELECT $fields FROM $table";
        $result = mysqli_query($this->con, $sql) or die(mysqli_error($this->con));
        if ($result->num_rows < 1) {
            return Errors::DB_NO_ROW_FOUND;
        }

        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

    /**
     * @param string $fields
     * @param string $table
     * @return string
     */
    public function fetch(string $fields,string $table): string|object
    {

        $sql = "SELECT $fields FROM $table";
        $result = mysqli_query($this->con, $sql) or die(mysqli_error($this->con));
        if ($result->num_rows < 1) {
            return Errors::DB_NO_ROW_FOUND;
        }

        return mysqli_fetch_object($result);
    }

    /**
     * @param int $id
     * @param string $table
     * @return bool
     */
    public function remove(int $id,string $table): bool
    {
        $sql = "delete FROM $table WHERE id=$id";

        return mysqli_query($this->con, $sql) or die(mysqli_error($this->con));
    }

   /**
   * @param array $data
   * @param string $table
   * @return bool|\mysqli_result|void
   */
    public function update(int $id, array $data, string $table)
    {
        $con = $this->con;
        $mysqliValue = '';
        $toEnd = count($data);

        foreach ($data as $key => $sqlValue) {
            if (0 === --$toEnd) {
                $mysqliValue .= $key.' = \'' . mysqli_real_escape_string($con, $sqlValue) . '\'';
            } else {
                $mysqliValue .= $key.' = \'' . mysqli_real_escape_string($con, $sqlValue) . '\', ';
            }
        }
        $sql = "UPDATE $table SET $mysqliValue WHERE id=$id;";
        $result = mysqli_query($this->con, $sql) or die(mysqli_error($this->con));

        return $result;
    }

  /**
   * @param BaseModel $object
   * @param string $tbl
   * @return bool|\mysqli_result
   */
    public function save(BaseModel $object,string $tbl): bool|\mysqli_result
    {
        $arraySql = (array) $object;

        $con = $this->con;
        $mysqliKey = "";
        $mysqliValue = "";
        foreach ($arraySql as $key => $sql_value) {
            $key = str_replace('*', '', $key);
            $key = preg_replace('/[\x00-\x1F\x7F]/', '', $key);
            $mysqliValue .= '\'' . mysqli_real_escape_string($con, $sql_value) . '\'';
            $mysqliValue .= ",";
            $mysqliKey .= $key;
            $mysqliKey .= ",";
        }
        $mysqliValue = rtrim($mysqliValue, ",");
        $mysqliKey = rtrim($mysqliKey, ",");
        $sql  = "INSERT INTO $tbl ($mysqliKey) VALUES ($mysqliValue)";

        $result = mysqli_query($this->con, $sql) or die(mysqli_error($this->con));

        return $result;
    }

    /**
     * @param $id
     * @param $table
     * @return bool|\mysqli_result|object|string|null
     */
    public function find(int $id,string $table)
    {
        $sql = "SELECT * FROM $table WHERE id = $id";
        $result = mysqli_query($this->con, $sql) or die(mysqli_error($this->con));
        if ($result->num_rows < 1) {
            return Errors::DB_NO_ROW_FOUND;
        }

        return mysqli_fetch_object($result);
    }

  /**
   * @param string $table
   * @param string $condition
   * @param $fields
   * @param $groupBy
   * @param $orderBy
   * @param $limit
   * @return array|string|void
   */
    public function findByCondition(string $table,string $fields,string $condition, $groupBy = null, $orderBy = null, $limit = null)
    {
        $sql = "SELECT $fields FROM $table WHERE $condition";
        $sql .= $groupBy ?? '';
        $sql .= $orderBy ?? '';
        $sql .= $limit ?? '';

        $result = mysqli_query($this->con, $sql) or die(mysqli_error($this->con));
        if ($result->num_rows < 1) {
            return Errors::DB_NO_ROW_FOUND;
        }

        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

    public function getLastId(): int
    {

      $con = $this->con;

      return mysqli_insert_id($con);
    }

    /**
     * @return bool
     */
    public function close()
    {
        return $this->con->close();
    }
}
