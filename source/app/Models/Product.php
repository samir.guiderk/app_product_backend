<?php

namespace CentralPointProductApp\Models;

use CentralPointProductApp\Contracts\BaseModel;

/**
 * The Product
 *
 * @category   Models
 * @author  Samir GUIDERK <samir.guiderk@gmail.com>
 * @copyright  2022 Centralpoint IO
 * @since      1.0.0
 *
 */
class Product implements BaseModel
{
  /**
   * @var int
   */
  protected int $id;

  /**
   * @var string
   */
  protected string $name;

  /**
   * @var string
   */
  protected string $brand;

  /**
   * @var string
   */
  protected float $price;

  /**
   * @var string
   */
  protected string $description;

  /**
   * @var string
   */
  protected int $stock;

  /**
   * @var string
   */
  protected string $createdDate;

  /**
   * @var string
   */
  protected string $updatedDate;

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name): void
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getBrand(): string
  {
    return $this->brand;
  }

  /**
   * @param string $brand
   */
  public function setBrand(string $brand): void
  {
    $this->brand = $brand;
  }

  /**
   * @return string
   */
  public function getPrice(): float|string
  {
    return $this->price;
  }

  /**
   * @param string $price
   */
  public function setPrice(float|string $price): void
  {
    $this->price = $price;
  }

  /**
   * @return string
   */
  public function getDescription(): string
  {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription(string $description): void
  {
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getStock(): int|string
  {
    return $this->stock;
  }

  /**
   * @param string $stock
   */
  public function setStock(int|string $stock): void
  {
    $this->stock = $stock;
  }

  /**
   * @return string
   */
  public function getCreatedDate(): string
  {
    return $this->createdDate;
  }

  /**
   * @param string $createdDate
   */
  public function setCreatedDate(string $createdDate): void
  {
    $this->createdDate = $createdDate;
  }

  /**
   * @return string
   */
  public function getUpdatedDate(): string
  {
    return $this->updatedDate;
  }

  /**
   * @param string $updatedDate
   */
  public function setUpdatedDate(string $updatedDate): void
  {
    $this->updatedDate = $updatedDate;
  }

  /**
   * @return int
   */
  public function getId(): int
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId(int $id): void
  {
    $this->id = $id;
  }
}
