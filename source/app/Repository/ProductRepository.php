<?php

namespace CentralPointProductApp\Repository;

use CentralPointProductApp\Constants\Errors;
use CentralPointProductApp\Contracts\ProductRepositoryInterface;
use CentralPointProductApp\Db\FactoryDB;
use CentralPointProductApp\Models\Product;

class ProductRepository implements ProductRepositoryInterface
{
  protected FactoryDB $db;
  protected string $fields = 'id,name,brand,description,price,stock';

  public function __construct(FactoryDB $db)
  {
    $this->db = $db;
  }

  /**
   * @return array|string
   */
  public function findAll(): array|string
  {
    try {
      // Find a record with the id = $id
      return $this->db->fetchAll($this->fields, 'product');
    } catch (\Exception $ex) {
      return Errors::SOMETHING_WRONG . $ex;
    }
  }

  /**
   * @param string $condition
   * @return array|string|void
   */
  public function findByCondition(string $condition)
  {
    try {
      // Find a record with the id = $id
      return $this->db->findByCondition('product', $this->fields, $condition);
    } catch (\Exception $ex) {
      return Errors::SOMETHING_WRONG . $ex;
    }
  }

  /**
   * @param $id
   */
  public function find($id)
  {
    try {
      // Find a record with the id = $id
      return $this->db->find($id, 'product');
    } catch (\Exception $ex) {
      return Errors::SOMETHING_WRONG . $ex;
    }
  }

  /**
   * @param $id
   * @param $data
   * @return string
   */
  public function update($id, $data): string
  {
    try {
      // Insert or update the $product
      $this->db->update($id, $data, 'product');

      return 'product updated';
    } catch (\Exception $ex) {
      return Errors::SOMETHING_WRONG . $ex;
    }
  }

  /**
   * @param Product $product
   * @return string
   */
  public function save(Product $product): string
  {
    try {
      // Insert or update the $product
      $this->db->save($product, 'product');
      return 'product inserted';
    } catch (\Exception $ex) {
      return Errors::SOMETHING_WRONG . $ex;
    }
  }

  /**
   * @param int $id
   * @return string
   */
  public function remove(int $id): string
  {
    try {
      // Remove the $product
      $this->db->remove($id, 'product');
      return 'product removed';
    } catch (\Exception $ex) {
      return Errors::SOMETHING_WRONG . $ex;
    }
  }
}
