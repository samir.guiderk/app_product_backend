<?php

namespace CentralPointProductAppTest\Services;

use CentralPointProductApp\Constants\Status;
use CentralPointProductApp\Db\FactoryDB;
use CentralPointProductApp\Repository\ProductRepository;
use CentralPointProductAppTest\UnitTest;
use GuzzleHttp\Client;
use http\Exception\InvalidArgumentException;

/**
 * The processUserTest
 *
 * @category  Services
 * @author  Samir GUIDERK <samir.guiderk@gmail.com>
 * @copyright  2022 Centralpoint IO
 * @since      1.0.0
 *
 */
class ApiProductTest extends UnitTest
{
  /**
   * Response.
   *
   * @var
   */
  protected ProductRepository $productRepository;
  protected Client $client;
  protected FactoryDB $factoryDB;

  private $http;

  public function __construct()
  {
    parent::__construct();
    $this->factoryDB = new FactoryDB();
    $this->productRepository = new ProductRepository($this->factoryDB);
    $this->http = new \GuzzleHttp\Client(['base_uri' => 'http://localhost:80/public/']);
  }

  public function test_get()
  {
    $response = $this->http->request('GET', 'index.php');

    $this->assertEquals(Status::CODE_SUCCESS, $response->getStatusCode());
    $contentType = $response->getHeaders()["Content-Type"][0];
    $this->assertEquals("application/json; charset=utf-8", $contentType);
  }

  public function test_get_users()
  {
    $response = $this->http->request('GET', 'index.php');

    $this->assertEquals(Status::CODE_SUCCESS, $response->getStatusCode());
  }

  public function test_post_user_422()
  {
    $response = $this->http->request('POST', 'index.php', ['http_errors' => false]);

    $this->assertEquals(Status::CODE_ERROR, $response->getStatusCode());
  }

  public function test_post_product()
  {
    $input = [
      'name' => 'Ipad',
      'brand' => 'Apple',
      'description' => 'description fr the product',
      'price' => 500,
      'stock' => 10,
    ];

    $response = $this->http->request('POST', 'index.php', [
      'form_params' => $input
    ]);

    $this->assertEquals(Status::CODE_SUCCESS_ADD, $response->getStatusCode());
  }

  public function test_can_find_product()
  {
    $response = $this->http->request('GET', 'index.php/2');
    $this->assertEquals(Status::CODE_SUCCESS, $response->getStatusCode());
    $contentType = $response->getHeaders()["Content-Type"][0];
    $this->assertEquals("application/json; charset=utf-8", $contentType);
  }

  public function test_can_edit_product()
  {
    $input = [
      'name' => 'Ipad for edit',
      'brand' => 'Apple',
      'description' => 'description for the product',
      'price' => 500,
      'stock' => 10,
    ];

    $response = $this->http->request('PUT', 'index.php/' . $this->factoryDB->getLastId(), [
      'form_params' => $input
    ]);
    $this->assertEquals(Status::CODE_SUCCESS, $response->getStatusCode());
    $contentType = $response->getHeaders()["Content-Type"][0];
    $this->assertEquals("application/json; charset=utf-8", $contentType);
  }

  public function test_can_delete_product()
  {
    $input = [
      'name' => 'Ipad',
      'brand' => 'Apple',
      'description' => 'description fr the product',
      'price' => 500,
      'stock' => 10,
    ];

    $this->http->request('POST', 'index.php', [
      'form_params' => $input
    ]);

    $response = $this->http->request('DELETE', 'index.php/' . $this->factoryDB->getLastId());
    $this->assertEquals(Status::CODE_SUCCESS, $response->getStatusCode());
    $contentType = $response->getHeaders()["Content-Type"][0];
    $this->assertEquals("application/json; charset=utf-8", $contentType);
  }

}
