<?php

namespace CentralPointProductAppTest\Services;

use CentralPointProductApp\Repository\ProductRepository;
use GuzzleHttp\Client;
use CentralPointProductApp\Db\FactoryDB;
use CentralPointProductApp\Models\Product;
use CentralPointProductAppTest\UnitTest;

/**
 * The processUserTest
 *
 * @category  Services
 * @author  Samir GUIDERK <samir.guiderk@gmail.com>
 * @copyright  2022 Centralpoint IO
 * @since      1.0.0
 *
 */
class ProcessProductTest extends UnitTest
{
    /**
     * Response.
     *
     * @var
     */
    protected ProductRepository $productRepository;
    protected Client $client;
    protected FactoryDB $factoryDB;


    public function __construct()
    {
        parent::__construct();
        $factoryDB = new FactoryDB();
        $this->productRepository = new ProductRepository($factoryDB);
    }

    public function test_can_add_user()
    {
      // Create new product model
      $product = new Product;
      $product->setName('Iphone');
      $product->setBrand('Apple');
      $product->setPrice(1000);
      $product->setDescription('description fr the product');
      $product->setStock(5);


      $result = $this->productRepository->save($product);
        $expectedResult = 'product inserted';
        $this->assertEquals($result, $expectedResult);
    }

    public function test_can_find_user()
    {
        $id = 1;

        $result = $this->productRepository->find($id);
        $array = json_decode(json_encode($result), true);
        $this->assertArrayHasKey('name', $array);
        $this->assertArrayHasKey('brand', $array);
        $this->assertArrayHasKey('price', $array);
    }

    public function test_can_delete_user()
    {
        $id = 1;

        $result = $this->productRepository->remove($id);
        $expectedResult = 'product removed';
        $this->assertEquals($result, $expectedResult);
    }

}
